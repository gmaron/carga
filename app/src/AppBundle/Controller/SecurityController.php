<?php 

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\User;
use AppBundle\Entity\Video;
use AppBundle\Controller\DefaultController;

class SecurityController extends Controller
{

	/**
     * @Route("/", name="def")
     */
	public function index (){
		
		$usr = new User();
		$usr->setUsername('gbatest');
		$pssw = password_hash("acercartegba", PASSWORD_DEFAULT);
		$usr->setPassword($pssw);
		$usr->setEmail('mercedesforte20@gmail.com');
		$repository = $this->getDoctrine()->getRepository('AppBundle:User');
		$em = $this->getDoctrine()->getManager();
		$usuario_ya_cargado = $repository->loadUserByUsername('gbatest');
		if ($usuario_ya_cargado == null){
			$em->persist($usr);
			$em->flush();
		}
		return $this->redirectToRoute('login');
	}

	/**
     * @Route("/login", name="login")
     */
	public function loginAction(Request $request)
	{		

		$user = $this->getUser();		

		if (is_object($user) || $user instanceof UserInterface){
			$this->cargarVideos();
			return $this->redirectToRoute("video_index");
		}

		$authenticationUtils = $this->get('security.authentication_utils');

		// get the login error if there is one
		$error = $authenticationUtils->getLastAuthenticationError();
		
		// last username entered by the user
		$lastUsername = $authenticationUtils->getLastUsername();

		return $this->render('default/login.html.twig', array(
			'last_username' => $lastUsername,
			'error'         => $error,
			));
	}
	
	/**
	 * @Route("/logout",name="logout")
	 */
	public function logoutAction() {
	}


	public function cargarVideos(){
		        $em = $this->getDoctrine()->getManager();

		$videos = $em->getRepository('AppBundle:Video')->findAll();

        if (sizeof($videos) == 0){
			$videos_en_js = array('https://www.youtube.com/watch?v=h9jxUSDKjo8',
    'https://www.youtube.com/watch?v=zTnRjGLP3Q0',
    'https://www.youtube.com/watch?v=MjZeXiacaTI',
    'https://www.youtube.com/watch?v=KEzqxzdipYs',
    'https://www.youtube.com/watch?v=UB41zKE4BtQ&t=2s',
    'https://www.youtube.com/watch?v=v7agAV8wAEY',
    'https://www.youtube.com/watch?v=092f2iaGgAE',
    'https://www.youtube.com/watch?v=E9KeVWXo6e8',
    'https://www.youtube.com/watch?v=EvRo8N_Dm_I',
    'https://www.youtube.com/watch?v=_Eumo7J5wgk&t=24s',
    'https://www.youtube.com/watch?v=qj_uXOqWY6Q&t=9s',
    'https://www.youtube.com/watch?v=gKQwyR4GLSM',
    'https://www.youtube.com/watch?v=QVvm4mpVMoo',
    'https://www.youtube.com/watch?v=HanRCiLJlfw',
    'https://www.youtube.com/watch?v=ToD-GHTNcKw',
    'https://www.youtube.com/watch?v=GaXVpAkrCZs',
    'https://www.youtube.com/watch?v=SQavbFPchRo',
    'https://www.youtube.com/watch?v=KwMXRXM_DS4',
    'https://www.youtube.com/watch?v=SVi53GVsui8',
    'https://www.youtube.com/watch?v=d1sgoImvOU0',
    'https://www.youtube.com/watch?v=SVi53GVsui8',
    'https://www.youtube.com/watch?v=d1sgoImvOU0',
    'https://www.youtube.com/watch?v=GsIlG961IZo',
    'https://www.youtube.com/watch?v=LdMt-He0nE4',
    'https://www.youtube.com/watch?v=PEi0S7A8GZA',
    'https://www.youtube.com/watch?v=mMjTKGDk1K4',
    // 'https://www.youtube.com/watch?v=KBRtKKuRgRw',
    'https://www.youtube.com/watch?v=KBRtKKuRgRw',
    'https://www.youtube.com/watch?v=Wxh5gKLt_Go',
    'https://www.youtube.com/watch?v=lu5bHSmrUo8',
    'https://www.youtube.com/watch?v=s_mo0S0SJMk',
    'https://www.youtube.com/watch?v=lu5bHSmrUo8',
    'https://www.youtube.com/watch?v=Ljn2h38hEek',
    'https://www.youtube.com/watch?v=I8zrsB-iytY',
    'https://www.youtube.com/watch?v=R7vmCr8cZSY',
    'https://www.youtube.com/watch?v=_fhWBUDHCI8',
    'https://www.youtube.com/watch?v=CGturIWUvNo',
    'https://www.youtube.com/watch?v=vMfs1IGs4vM',
    'https://www.youtube.com/watch?v=mEhQCREjv9s',
    'https://www.youtube.com/watch?v=MYc4lx1OV_Q',
    'https://www.youtube.com/watch?v=_0v5d6DhdwE',
    // 'https://www.youtube.com/watch?v=aVQmkwe2It0',
    'https://www.youtube.com/watch?v=aVQmkwe2It0',
    'https://www.youtube.com/watch?v=gg2z8WSw5fc',
    'https://www.youtube.com/watch?v=Lm_9M_fyFbA',
    'https://www.youtube.com/watch?v=xFac06ofvuo',
    'https://www.youtube.com/watch?v=t1ADlHGVMjw',
    'https://www.youtube.com/watch?v=S3f-GtiEFbE',
    'https://www.youtube.com/watch?v=rm79HoFhk2k',
    'https://www.youtube.com/watch?v=3lsu-MInzJI',
    'https://www.youtube.com/watch?v=6GxLNxA-AK8',
    'https://www.youtube.com/watch?v=M5j3K57O6Rs',
    'https://www.youtube.com/watch?v=bz-NgPWZ9JM',
    'https://www.youtube.com/watch?v=rurMhTG1KAY');

			$videos_sin_duplicados = array_unique($videos_en_js, SORT_STRING);
			$em = $this->getDoctrine()->getManager();

			foreach ( array_reverse($videos_sin_duplicados) as $key => $value) {
  				$video = new Video();
				$video->setUrl($value);
				$em->persist($video);
			}		
			
			$em->flush();
		}
	}

}
?>