<?php
namespace AppBundle\APIController;

use Symfony\Component\HttpFoundation\Response;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Doctrine\ORM\QueryBuilder;
use AppBundle\Entity\Video;

class APIVideoController extends Controller{
	/**
     * Note: here the name is important
     * get => the action is restricted to GET HTTP method
     * Article => (without s) generate /articles/SOMETHING
     * Action => standard things for symfony for a controller method which
     *           generate an output
     *
     * it generates so the route GET .../articles/{id}
     */
    public function getVideoAction()
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('AppBundle:Video')->findAll();
        $videos = array();
        foreach ($qb as $key => $value) {
            $videos[$key] = $value->getUrl();
        }
        $response = new Response(json_encode($videos));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}

  ?>